const images = document.querySelectorAll('.image-to-show');
let currentIndex = 0;
let showImage;
function swapActive() {
    showImage = setInterval(function timer() {
        images[currentIndex].classList.remove("active");
        console.log(currentIndex);
        currentIndex = (currentIndex + 1) % images.length;
        images[currentIndex].classList.add("active");
        console.log(currentIndex);
    }, 3000);
}

swapActive();
console.log(currentIndex);

let btnStop = document.querySelector("#btn-stop");
let btnCon = document.querySelector("#btn-con");
btnCon.disabled = true;

btnStop.addEventListener("click", function () {
    clearInterval(showImage);
    btnCon.disabled = false;
});

btnCon.addEventListener("click", function () {
    clearInterval(showImage);
    swapActive();
    btnCon.disabled = true;
});


