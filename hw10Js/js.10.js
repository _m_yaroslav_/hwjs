//Отримуємо всі вкладки та контент вкладок з DOM за допомогою методу querySelectorAll.
const tabs = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');

//Запускаємо цикл по всім вкладкам, додаємо до них обробник подій 'click'.
tabs.forEach((tab, index) => {
    tab.addEventListener('click', () => {
// Спочатку ховаємо всі текстові блоки
        tabContents.forEach(content => {     //forEach по всім елементам в tabs кожен елемент відповідає за одну вкладку
            content.style.display = 'none';  //none це для того, щоб показувати тільки той блок контенту, який відповідає вибраній вкладці.
        });
// Показуємо відповідний текстовий блок для вибраної вкладки
        tabContents [index].style.display = 'block';

// Змінюємо активний клас на вибраній вкладці та видаляємо його з інших вкладок (допомагає користувачеві побачити, яка вкладка є активною.)
        tabs.forEach(tab => {
            tab.classList.remove('active');

        });
        tabs.forEach(tab => {
            tab.classList.remove('active');
        });
        tab.classList.add('active');
    });
});

