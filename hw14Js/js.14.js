function toggleTheme() {  //toggleTheme() переключает класс dark на элементе body в DOM-дереве
    // также устанавливает значение переменной isDarkMode в зависимости от того, содержит ли элемент body класс dark после переключения.
    const body = document.querySelector('body');
    body.classList.toggle('dark');
    const isDarkMode = body.classList.contains('dark');
    localStorage.setItem('isDarkMode', isDarkMode);
}

const savedTheme = localStorage.getItem('isDarkMode');
if (savedTheme) {
    const body = document.querySelector('body');
    const isDarkMode = savedTheme === 'true';
    body.classList.toggle('dark', isDarkMode);
}
