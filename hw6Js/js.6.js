function createNewUser() {
    const newUser = {
        firstName: '',
        lastName: '',
        birthday: '',
        getAge() {
            const today = new Date();
            const birthDate = new Date(this.birthday);
            let age = today.getFullYear() - birthDate.getFullYear();
            const month = today.getMonth() - birthDate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        getPassword() {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);


        },
    };

    newUser.firstName = prompt('Введіть ім\'я:', '');
    newUser.lastName = prompt('Введіть прізвище:', '');
    newUser.birthday = prompt('Введіть дату народження у форматі dd.mm.yyyy:', '');

    console.log('Створений користувач:', newUser);
    console.log('Вік користувача:', newUser.getAge());
    console.log('Пароль користувача:', newUser.getPassword());

    return newUser;
}

createNewUser();


/*this.firstName.charAt(0).toUpperCase()
 повертає першу літеру імені користувача у верхньому регістрі,
 використовуючи метод charAt() для отримання першого символу зі строки firstName,
та метод toUpperCase() для перетворення символу у верхній регістр.

Вираз this.birthday.slice(-4) повертає чотири останні цифри з рядка birthday,
 використовуючи метод slice() для вибору останніх чотирьох символів.
 */




/*
Теоретичні питання

1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
2.Які засоби оголошення функцій ви знаєте?
3.Що таке hoisting, як він працює для змінних та функцій?


1.Екранування в мовах програмування - це процес включення спеціальних символів в рядки,
 які в іншому випадку можуть бути сприйняті як частини синтаксичної конструкції, а не просто символи.

2.Оголошення функції (Function Declaration)
Функціональний вираз (Function Expression)
Стрілкові функції (Arrow Functions)

3.Hoisting - це механізм в JavaScript, який дозволяє оголошувати змінні
 та функції після їх використання в коді. Під час компіляції JavaScript-коду,
 всі оголошення змінних та функцій переміщаються вверх відносно їх використання,
 тому що ці змінні та функції вважаються вже оголошеними.

 */