const passwordInputs = document.querySelectorAll('input[type="password"]');
const passwordIcons = document.querySelectorAll('.icon-password');
passwordIcons.forEach(icon => {
    icon.addEventListener('click', () => {
        const passwordInput = icon.previousElementSibling;
        const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordInput.setAttribute('type', type);
        icon.classList.toggle('fa-eye-slash');
    });
});
const submitBtn = document.querySelector('.btn');
submitBtn.addEventListener('click', event => {
    event.preventDefault();
    const firstPassword = passwordInputs[0].value;
    const secondPassword = passwordInputs[1].value;
    if (firstPassword === secondPassword) {
        alert('You are welcome');
    } else {
        const errorText = document.createElement('span');
        errorText.textContent = 'Потрібно ввести однакові значення';
        errorText.style.color = 'red';
        passwordInputs[1].insertAdjacentElement('afterend', errorText);
    }
});
